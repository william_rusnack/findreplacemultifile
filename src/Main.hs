{-# LANGUAGE LambdaCase
           , ViewPatterns #-}

module Main where

import Control.Arrow
import Control.Monad
import Data.Maybe
import Data.Text (pack, unpack, intercalate, splitOn)
import Data.Traversable
import System.Directory
import System.Directory.Internal
import System.Environment
import System.FilePath.Posix

import Safe


frdVar :: String
frdVar = "FIND_REPLACE_DIR"

instructions :: String
instructions =
  "Find and Replace in multiple files.\n\
  \> fr [rootDir] \"searchPattern\" \"replacementString\"\n\n\
  \If `rootDir` is not included then the env var`" ++ frdVar ++ "`must be set.\n" ++
  exportCmd ++ "\n"

setFrdVar :: String
setFrdVar =
  "Set env var `" ++ frdVar ++ "` to the root directory path for searching.\n" ++
  exportCmd

exportCmd :: String
exportCmd = "> export " ++ frdVar ++ "=\"/path/to/root/dir\""

isHidden :: String -> Bool
isHidden = (== Just '.') . headMay

rFileList :: FilePath -> IO [FilePath]
rFileList fp = do
  isDir <- (== Directory) . fileTypeFromMetadata <$> getFileMetadata fp
  if isDir
    then do
      fps <- fmap (combine fp) . filter (not . isHidden) <$> listDirectory fp
      concat <$> traverse rFileList fps
    else return [fp]

iff :: (a -> Bool) -> (a -> b) -> (a -> b) -> a -> b
iff b f0 f1 v = if b v then f0 v else f1 v

infixr 4 <&>
(<&>) :: Functor f => f a -> (a -> b) -> f b
(<&>) = flip (<$>)


main :: IO ()
main =
  getArgs >>=
  (\case
    [rootDir, pattern, replacement] -> return $ Right (rootDir, pattern, replacement)
    [pattern, replacement] ->
      lookupEnv frdVar <&>
      (\case Nothing -> Left setFrdVar
             Just rootDir -> if not $ null rootDir
                              then Right (rootDir, pattern, replacement)
                              else Left setFrdVar )
    -- args | any (`elem` args) ["-h", "--help"] -> Left instructions
    _ -> return $ Left instructions ) >>=
  (\case Left msg -> return $ Left msg
         Right rpr@(rootDir, _, _) ->
           doesDirectoryExist rootDir >>=
           \case False -> return $ Left $ "Invalid root dir: " ++ rootDir ++ "\nChange root dir with:\n" ++ exportCmd
                 True -> return $ Right rpr) >>=
  (\case Left msg -> return $ Left msg
         Right (rootDir, pack -> pattern, pack -> replacement) -> do
           putStrLn $ "Root is: " ++ rootDir

           changedPaths <-
             rFileList rootDir >>=
             traverse (\fp ->
                        readFile fp >>=
                        ( pack >>>
                          splitOn pattern >>>
                          iff
                              (length >>> (<= 1))
                              (const $ return Nothing)
                              ( ( intercalate replacement >>>
                                  unpack >>>
                                  writeFile fp ) >=>
                                (const $ return $ Just fp)
                         ) ) ) >>=
             return . catMaybes

           when (length changedPaths > 0) $ putStrLn "Changed Files:"
           _ <- for changedPaths $ (++) "  " >>> putStrLn

           putStrLn $ show (length changedPaths) ++ " Files Changed" 

           return $ Right () ) >>=
  (\case Right _ -> return ()
         Left msg -> putStrLn msg )

